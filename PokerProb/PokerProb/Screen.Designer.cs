﻿namespace PokerProb
{
    partial class Screen
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Screen));
            this.tabella1 = new System.Windows.Forms.TableLayoutPanel();
            this.carta2 = new System.Windows.Forms.ComboBox();
            this.carta1 = new System.Windows.Forms.ComboBox();
            this.seme1 = new System.Windows.Forms.ComboBox();
            this.calcola = new System.Windows.Forms.Button();
            this.testo1 = new System.Windows.Forms.Label();
            this.seme2 = new System.Windows.Forms.ComboBox();
            this.tabella2 = new System.Windows.Forms.TableLayoutPanel();
            this.carta7 = new System.Windows.Forms.ComboBox();
            this.carta6 = new System.Windows.Forms.ComboBox();
            this.carta5 = new System.Windows.Forms.ComboBox();
            this.carta4 = new System.Windows.Forms.ComboBox();
            this.carta3 = new System.Windows.Forms.ComboBox();
            this.seme3 = new System.Windows.Forms.ComboBox();
            this.resetta = new System.Windows.Forms.Button();
            this.testo2 = new System.Windows.Forms.Label();
            this.seme4 = new System.Windows.Forms.ComboBox();
            this.seme5 = new System.Windows.Forms.ComboBox();
            this.seme6 = new System.Windows.Forms.ComboBox();
            this.seme7 = new System.Windows.Forms.ComboBox();
            this.random = new System.Windows.Forms.Button();
            this.tabella3 = new System.Windows.Forms.TableLayoutPanel();
            this.testo19 = new System.Windows.Forms.Label();
            this.testo18 = new System.Windows.Forms.Label();
            this.testo17 = new System.Windows.Forms.Label();
            this.testo16 = new System.Windows.Forms.Label();
            this.testo15 = new System.Windows.Forms.Label();
            this.testo14 = new System.Windows.Forms.Label();
            this.testo13 = new System.Windows.Forms.Label();
            this.testo12 = new System.Windows.Forms.Label();
            this.testo11 = new System.Windows.Forms.Label();
            this.testo3 = new System.Windows.Forms.Label();
            this.testo00 = new System.Windows.Forms.Label();
            this.testo01 = new System.Windows.Forms.Label();
            this.testo02 = new System.Windows.Forms.Label();
            this.testo03 = new System.Windows.Forms.Label();
            this.testo04 = new System.Windows.Forms.Label();
            this.testo05 = new System.Windows.Forms.Label();
            this.testo06 = new System.Windows.Forms.Label();
            this.testo07 = new System.Windows.Forms.Label();
            this.testo08 = new System.Windows.Forms.Label();
            this.testo09 = new System.Windows.Forms.Label();
            this.testo10 = new System.Windows.Forms.Label();
            this.testo4 = new System.Windows.Forms.Label();
            this.testo20 = new System.Windows.Forms.Label();
            this.testo21 = new System.Windows.Forms.Label();
            this.testo22 = new System.Windows.Forms.Label();
            this.testo23 = new System.Windows.Forms.Label();
            this.testo24 = new System.Windows.Forms.Label();
            this.testo25 = new System.Windows.Forms.Label();
            this.testo27 = new System.Windows.Forms.Label();
            this.testo28 = new System.Windows.Forms.Label();
            this.testo29 = new System.Windows.Forms.Label();
            this.testo26 = new System.Windows.Forms.Label();
            this.tabella1.SuspendLayout();
            this.tabella2.SuspendLayout();
            this.tabella3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabella1
            // 
            this.tabella1.ColumnCount = 4;
            this.tabella1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tabella1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tabella1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tabella1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tabella1.Controls.Add(this.carta2, 2, 0);
            this.tabella1.Controls.Add(this.carta1, 1, 0);
            this.tabella1.Controls.Add(this.seme1, 1, 1);
            this.tabella1.Controls.Add(this.calcola, 3, 0);
            this.tabella1.Controls.Add(this.testo1, 0, 0);
            this.tabella1.Controls.Add(this.seme2, 2, 1);
            this.tabella1.Location = new System.Drawing.Point(12, 12);
            this.tabella1.Name = "tabella1";
            this.tabella1.RowCount = 2;
            this.tabella1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tabella1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tabella1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella1.Size = new System.Drawing.Size(360, 58);
            this.tabella1.TabIndex = 0;
            // 
            // carta2
            // 
            this.carta2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.carta2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.carta2.FormattingEnabled = true;
            this.carta2.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "J",
            "Q",
            "K",
            "A"});
            this.carta2.Location = new System.Drawing.Point(183, 3);
            this.carta2.Name = "carta2";
            this.carta2.Size = new System.Drawing.Size(84, 21);
            this.carta2.TabIndex = 7;
            // 
            // carta1
            // 
            this.carta1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.carta1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.carta1.FormattingEnabled = true;
            this.carta1.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "J",
            "Q",
            "K",
            "A"});
            this.carta1.Location = new System.Drawing.Point(93, 3);
            this.carta1.Name = "carta1";
            this.carta1.Size = new System.Drawing.Size(84, 21);
            this.carta1.TabIndex = 6;
            // 
            // seme1
            // 
            this.seme1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seme1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.seme1.FormattingEnabled = true;
            this.seme1.Items.AddRange(new object[] {
            "Cuori",
            "Quadri",
            "Fiori",
            "Picche"});
            this.seme1.Location = new System.Drawing.Point(93, 32);
            this.seme1.Name = "seme1";
            this.seme1.Size = new System.Drawing.Size(84, 21);
            this.seme1.TabIndex = 1;
            // 
            // calcola
            // 
            this.calcola.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.calcola.Location = new System.Drawing.Point(273, 3);
            this.calcola.Name = "calcola";
            this.calcola.Size = new System.Drawing.Size(84, 23);
            this.calcola.TabIndex = 4;
            this.calcola.Text = "Calcola";
            this.calcola.UseVisualStyleBackColor = true;
            this.calcola.Click += new System.EventHandler(this.calcola_Click);
            // 
            // testo1
            // 
            this.testo1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo1.AutoSize = true;
            this.testo1.Location = new System.Drawing.Point(3, 0);
            this.testo1.Name = "testo1";
            this.testo1.Size = new System.Drawing.Size(84, 29);
            this.testo1.TabIndex = 0;
            this.testo1.Text = "Carte Giocatore";
            this.testo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // seme2
            // 
            this.seme2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seme2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.seme2.FormattingEnabled = true;
            this.seme2.Items.AddRange(new object[] {
            "Cuori",
            "Quadri",
            "Fiori",
            "Picche"});
            this.seme2.Location = new System.Drawing.Point(183, 32);
            this.seme2.Name = "seme2";
            this.seme2.Size = new System.Drawing.Size(84, 21);
            this.seme2.TabIndex = 5;
            // 
            // tabella2
            // 
            this.tabella2.ColumnCount = 5;
            this.tabella2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tabella2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tabella2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tabella2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tabella2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tabella2.Controls.Add(this.carta7, 4, 1);
            this.tabella2.Controls.Add(this.carta6, 3, 1);
            this.tabella2.Controls.Add(this.carta5, 2, 1);
            this.tabella2.Controls.Add(this.carta4, 1, 1);
            this.tabella2.Controls.Add(this.carta3, 0, 1);
            this.tabella2.Controls.Add(this.seme3, 0, 2);
            this.tabella2.Controls.Add(this.resetta, 4, 0);
            this.tabella2.Controls.Add(this.testo2, 0, 0);
            this.tabella2.Controls.Add(this.seme4, 1, 2);
            this.tabella2.Controls.Add(this.seme5, 2, 2);
            this.tabella2.Controls.Add(this.seme6, 3, 2);
            this.tabella2.Controls.Add(this.seme7, 4, 2);
            this.tabella2.Controls.Add(this.random, 2, 0);
            this.tabella2.Location = new System.Drawing.Point(12, 76);
            this.tabella2.Name = "tabella2";
            this.tabella2.RowCount = 3;
            this.tabella2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tabella2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tabella2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tabella2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella2.Size = new System.Drawing.Size(360, 100);
            this.tabella2.TabIndex = 1;
            // 
            // carta7
            // 
            this.carta7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.carta7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.carta7.FormattingEnabled = true;
            this.carta7.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "J",
            "Q",
            "K",
            "A"});
            this.carta7.Location = new System.Drawing.Point(291, 36);
            this.carta7.Name = "carta7";
            this.carta7.Size = new System.Drawing.Size(66, 21);
            this.carta7.TabIndex = 22;
            // 
            // carta6
            // 
            this.carta6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.carta6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.carta6.FormattingEnabled = true;
            this.carta6.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "J",
            "Q",
            "K",
            "A"});
            this.carta6.Location = new System.Drawing.Point(219, 36);
            this.carta6.Name = "carta6";
            this.carta6.Size = new System.Drawing.Size(66, 21);
            this.carta6.TabIndex = 21;
            // 
            // carta5
            // 
            this.carta5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.carta5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.carta5.FormattingEnabled = true;
            this.carta5.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "J",
            "Q",
            "K",
            "A"});
            this.carta5.Location = new System.Drawing.Point(147, 36);
            this.carta5.Name = "carta5";
            this.carta5.Size = new System.Drawing.Size(66, 21);
            this.carta5.TabIndex = 20;
            // 
            // carta4
            // 
            this.carta4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.carta4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.carta4.FormattingEnabled = true;
            this.carta4.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "J",
            "Q",
            "K",
            "A"});
            this.carta4.Location = new System.Drawing.Point(75, 36);
            this.carta4.Name = "carta4";
            this.carta4.Size = new System.Drawing.Size(66, 21);
            this.carta4.TabIndex = 19;
            // 
            // carta3
            // 
            this.carta3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.carta3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.carta3.FormattingEnabled = true;
            this.carta3.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "J",
            "Q",
            "K",
            "A"});
            this.carta3.Location = new System.Drawing.Point(3, 36);
            this.carta3.Name = "carta3";
            this.carta3.Size = new System.Drawing.Size(66, 21);
            this.carta3.TabIndex = 18;
            // 
            // seme3
            // 
            this.seme3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seme3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.seme3.FormattingEnabled = true;
            this.seme3.Items.AddRange(new object[] {
            "Cuori",
            "Quadri",
            "Fiori",
            "Picche"});
            this.seme3.Location = new System.Drawing.Point(3, 69);
            this.seme3.Name = "seme3";
            this.seme3.Size = new System.Drawing.Size(66, 21);
            this.seme3.TabIndex = 6;
            // 
            // resetta
            // 
            this.resetta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resetta.Location = new System.Drawing.Point(291, 3);
            this.resetta.Name = "resetta";
            this.resetta.Size = new System.Drawing.Size(66, 27);
            this.resetta.TabIndex = 12;
            this.resetta.Text = "Reset";
            this.resetta.UseVisualStyleBackColor = true;
            this.resetta.Click += new System.EventHandler(this.resetta_Click);
            // 
            // testo2
            // 
            this.testo2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo2.AutoSize = true;
            this.testo2.Location = new System.Drawing.Point(3, 0);
            this.testo2.Name = "testo2";
            this.testo2.Size = new System.Drawing.Size(66, 33);
            this.testo2.TabIndex = 1;
            this.testo2.Text = "Tavolo";
            this.testo2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // seme4
            // 
            this.seme4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seme4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.seme4.FormattingEnabled = true;
            this.seme4.Items.AddRange(new object[] {
            "Cuori",
            "Quadri",
            "Fiori",
            "Picche"});
            this.seme4.Location = new System.Drawing.Point(75, 69);
            this.seme4.Name = "seme4";
            this.seme4.Size = new System.Drawing.Size(66, 21);
            this.seme4.TabIndex = 13;
            // 
            // seme5
            // 
            this.seme5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seme5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.seme5.FormattingEnabled = true;
            this.seme5.Items.AddRange(new object[] {
            "Cuori",
            "Quadri",
            "Fiori",
            "Picche"});
            this.seme5.Location = new System.Drawing.Point(147, 69);
            this.seme5.Name = "seme5";
            this.seme5.Size = new System.Drawing.Size(66, 21);
            this.seme5.TabIndex = 14;
            // 
            // seme6
            // 
            this.seme6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seme6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.seme6.FormattingEnabled = true;
            this.seme6.Items.AddRange(new object[] {
            "Cuori",
            "Quadri",
            "Fiori",
            "Picche"});
            this.seme6.Location = new System.Drawing.Point(219, 69);
            this.seme6.Name = "seme6";
            this.seme6.Size = new System.Drawing.Size(66, 21);
            this.seme6.TabIndex = 15;
            // 
            // seme7
            // 
            this.seme7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seme7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.seme7.FormattingEnabled = true;
            this.seme7.Items.AddRange(new object[] {
            "Cuori",
            "Quadri",
            "Fiori",
            "Picche"});
            this.seme7.Location = new System.Drawing.Point(291, 69);
            this.seme7.Name = "seme7";
            this.seme7.Size = new System.Drawing.Size(66, 21);
            this.seme7.TabIndex = 16;
            // 
            // random
            // 
            this.random.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.random.Location = new System.Drawing.Point(147, 3);
            this.random.Name = "random";
            this.random.Size = new System.Drawing.Size(66, 27);
            this.random.TabIndex = 45;
            this.random.Text = "Random";
            this.random.UseVisualStyleBackColor = true;
            this.random.Click += new System.EventHandler(this.random_Click);
            // 
            // tabella3
            // 
            this.tabella3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tabella3.ColumnCount = 3;
            this.tabella3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tabella3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tabella3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tabella3.Controls.Add(this.testo26, 2, 7);
            this.tabella3.Controls.Add(this.testo19, 1, 10);
            this.tabella3.Controls.Add(this.testo18, 1, 9);
            this.tabella3.Controls.Add(this.testo17, 1, 8);
            this.tabella3.Controls.Add(this.testo16, 1, 7);
            this.tabella3.Controls.Add(this.testo15, 1, 6);
            this.tabella3.Controls.Add(this.testo14, 1, 5);
            this.tabella3.Controls.Add(this.testo13, 1, 4);
            this.tabella3.Controls.Add(this.testo12, 1, 3);
            this.tabella3.Controls.Add(this.testo11, 1, 2);
            this.tabella3.Controls.Add(this.testo3, 1, 0);
            this.tabella3.Controls.Add(this.testo00, 0, 1);
            this.tabella3.Controls.Add(this.testo01, 0, 2);
            this.tabella3.Controls.Add(this.testo02, 0, 3);
            this.tabella3.Controls.Add(this.testo03, 0, 4);
            this.tabella3.Controls.Add(this.testo04, 0, 5);
            this.tabella3.Controls.Add(this.testo05, 0, 6);
            this.tabella3.Controls.Add(this.testo06, 0, 7);
            this.tabella3.Controls.Add(this.testo07, 0, 8);
            this.tabella3.Controls.Add(this.testo08, 0, 9);
            this.tabella3.Controls.Add(this.testo09, 0, 10);
            this.tabella3.Controls.Add(this.testo10, 1, 1);
            this.tabella3.Controls.Add(this.testo4, 2, 0);
            this.tabella3.Controls.Add(this.testo20, 2, 1);
            this.tabella3.Controls.Add(this.testo21, 2, 2);
            this.tabella3.Controls.Add(this.testo22, 2, 3);
            this.tabella3.Controls.Add(this.testo23, 2, 4);
            this.tabella3.Controls.Add(this.testo24, 2, 5);
            this.tabella3.Controls.Add(this.testo25, 2, 6);
            this.tabella3.Controls.Add(this.testo27, 2, 8);
            this.tabella3.Controls.Add(this.testo28, 2, 9);
            this.tabella3.Controls.Add(this.testo29, 2, 10);
            this.tabella3.Location = new System.Drawing.Point(12, 182);
            this.tabella3.Name = "tabella3";
            this.tabella3.RowCount = 11;
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tabella3.Size = new System.Drawing.Size(360, 232);
            this.tabella3.TabIndex = 2;
            // 
            // testo19
            // 
            this.testo19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo19.AutoSize = true;
            this.testo19.Location = new System.Drawing.Point(111, 211);
            this.testo19.Name = "testo19";
            this.testo19.Size = new System.Drawing.Size(118, 20);
            this.testo19.TabIndex = 44;
            this.testo19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo18
            // 
            this.testo18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo18.AutoSize = true;
            this.testo18.Location = new System.Drawing.Point(111, 190);
            this.testo18.Name = "testo18";
            this.testo18.Size = new System.Drawing.Size(118, 20);
            this.testo18.TabIndex = 42;
            this.testo18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo17
            // 
            this.testo17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo17.AutoSize = true;
            this.testo17.Location = new System.Drawing.Point(111, 169);
            this.testo17.Name = "testo17";
            this.testo17.Size = new System.Drawing.Size(118, 20);
            this.testo17.TabIndex = 40;
            this.testo17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo16
            // 
            this.testo16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo16.AutoSize = true;
            this.testo16.Location = new System.Drawing.Point(111, 148);
            this.testo16.Name = "testo16";
            this.testo16.Size = new System.Drawing.Size(118, 20);
            this.testo16.TabIndex = 38;
            this.testo16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo15
            // 
            this.testo15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo15.AutoSize = true;
            this.testo15.Location = new System.Drawing.Point(111, 127);
            this.testo15.Name = "testo15";
            this.testo15.Size = new System.Drawing.Size(118, 20);
            this.testo15.TabIndex = 36;
            this.testo15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo14
            // 
            this.testo14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo14.AutoSize = true;
            this.testo14.Location = new System.Drawing.Point(111, 106);
            this.testo14.Name = "testo14";
            this.testo14.Size = new System.Drawing.Size(118, 20);
            this.testo14.TabIndex = 34;
            this.testo14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo13
            // 
            this.testo13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo13.AutoSize = true;
            this.testo13.Location = new System.Drawing.Point(111, 85);
            this.testo13.Name = "testo13";
            this.testo13.Size = new System.Drawing.Size(118, 20);
            this.testo13.TabIndex = 32;
            this.testo13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo12
            // 
            this.testo12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo12.AutoSize = true;
            this.testo12.Location = new System.Drawing.Point(111, 64);
            this.testo12.Name = "testo12";
            this.testo12.Size = new System.Drawing.Size(118, 20);
            this.testo12.TabIndex = 30;
            this.testo12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo11
            // 
            this.testo11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo11.AutoSize = true;
            this.testo11.Location = new System.Drawing.Point(111, 43);
            this.testo11.Name = "testo11";
            this.testo11.Size = new System.Drawing.Size(118, 20);
            this.testo11.TabIndex = 28;
            this.testo11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo3
            // 
            this.testo3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo3.AutoSize = true;
            this.testo3.Location = new System.Drawing.Point(111, 1);
            this.testo3.Name = "testo3";
            this.testo3.Size = new System.Drawing.Size(118, 20);
            this.testo3.TabIndex = 13;
            this.testo3.Text = "Probabilità %";
            this.testo3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo00
            // 
            this.testo00.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo00.AutoSize = true;
            this.testo00.Location = new System.Drawing.Point(4, 22);
            this.testo00.Name = "testo00";
            this.testo00.Size = new System.Drawing.Size(100, 20);
            this.testo00.TabIndex = 15;
            this.testo00.Text = "Carta Alta";
            this.testo00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo01
            // 
            this.testo01.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo01.AutoSize = true;
            this.testo01.Location = new System.Drawing.Point(4, 43);
            this.testo01.Name = "testo01";
            this.testo01.Size = new System.Drawing.Size(100, 20);
            this.testo01.TabIndex = 16;
            this.testo01.Text = "Coppia";
            this.testo01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo02
            // 
            this.testo02.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo02.AutoSize = true;
            this.testo02.Location = new System.Drawing.Point(4, 64);
            this.testo02.Name = "testo02";
            this.testo02.Size = new System.Drawing.Size(100, 20);
            this.testo02.TabIndex = 17;
            this.testo02.Text = "Doppia Coppia";
            this.testo02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo03
            // 
            this.testo03.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo03.AutoSize = true;
            this.testo03.Location = new System.Drawing.Point(4, 85);
            this.testo03.Name = "testo03";
            this.testo03.Size = new System.Drawing.Size(100, 20);
            this.testo03.TabIndex = 18;
            this.testo03.Text = "Tris";
            this.testo03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo04
            // 
            this.testo04.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo04.AutoSize = true;
            this.testo04.Location = new System.Drawing.Point(4, 106);
            this.testo04.Name = "testo04";
            this.testo04.Size = new System.Drawing.Size(100, 20);
            this.testo04.TabIndex = 19;
            this.testo04.Text = "Scala";
            this.testo04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo05
            // 
            this.testo05.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo05.AutoSize = true;
            this.testo05.Location = new System.Drawing.Point(4, 127);
            this.testo05.Name = "testo05";
            this.testo05.Size = new System.Drawing.Size(100, 20);
            this.testo05.TabIndex = 20;
            this.testo05.Text = "Colore";
            this.testo05.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo06
            // 
            this.testo06.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo06.AutoSize = true;
            this.testo06.Location = new System.Drawing.Point(4, 148);
            this.testo06.Name = "testo06";
            this.testo06.Size = new System.Drawing.Size(100, 20);
            this.testo06.TabIndex = 21;
            this.testo06.Text = "Full";
            this.testo06.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo07
            // 
            this.testo07.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo07.AutoSize = true;
            this.testo07.Location = new System.Drawing.Point(4, 169);
            this.testo07.Name = "testo07";
            this.testo07.Size = new System.Drawing.Size(100, 20);
            this.testo07.TabIndex = 22;
            this.testo07.Text = "Poker";
            this.testo07.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo08
            // 
            this.testo08.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo08.AutoSize = true;
            this.testo08.Location = new System.Drawing.Point(4, 190);
            this.testo08.Name = "testo08";
            this.testo08.Size = new System.Drawing.Size(100, 20);
            this.testo08.TabIndex = 23;
            this.testo08.Text = "Scala Colore";
            this.testo08.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo09
            // 
            this.testo09.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo09.AutoSize = true;
            this.testo09.Location = new System.Drawing.Point(4, 211);
            this.testo09.Name = "testo09";
            this.testo09.Size = new System.Drawing.Size(100, 20);
            this.testo09.TabIndex = 24;
            this.testo09.Text = "Scala Reale";
            this.testo09.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo10
            // 
            this.testo10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo10.AutoSize = true;
            this.testo10.Location = new System.Drawing.Point(111, 22);
            this.testo10.Name = "testo10";
            this.testo10.Size = new System.Drawing.Size(118, 20);
            this.testo10.TabIndex = 25;
            this.testo10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo4
            // 
            this.testo4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo4.AutoSize = true;
            this.testo4.Location = new System.Drawing.Point(236, 1);
            this.testo4.Name = "testo4";
            this.testo4.Size = new System.Drawing.Size(120, 20);
            this.testo4.TabIndex = 45;
            this.testo4.Text = "Casi Favorevoli";
            this.testo4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo20
            // 
            this.testo20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo20.AutoSize = true;
            this.testo20.Location = new System.Drawing.Point(236, 22);
            this.testo20.Name = "testo20";
            this.testo20.Size = new System.Drawing.Size(120, 20);
            this.testo20.TabIndex = 46;
            this.testo20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo21
            // 
            this.testo21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo21.AutoSize = true;
            this.testo21.Location = new System.Drawing.Point(236, 43);
            this.testo21.Name = "testo21";
            this.testo21.Size = new System.Drawing.Size(120, 20);
            this.testo21.TabIndex = 47;
            this.testo21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo22
            // 
            this.testo22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo22.AutoSize = true;
            this.testo22.Location = new System.Drawing.Point(236, 64);
            this.testo22.Name = "testo22";
            this.testo22.Size = new System.Drawing.Size(120, 20);
            this.testo22.TabIndex = 48;
            this.testo22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo23
            // 
            this.testo23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo23.AutoSize = true;
            this.testo23.Location = new System.Drawing.Point(236, 85);
            this.testo23.Name = "testo23";
            this.testo23.Size = new System.Drawing.Size(120, 20);
            this.testo23.TabIndex = 49;
            this.testo23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo24
            // 
            this.testo24.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo24.AutoSize = true;
            this.testo24.Location = new System.Drawing.Point(236, 106);
            this.testo24.Name = "testo24";
            this.testo24.Size = new System.Drawing.Size(120, 20);
            this.testo24.TabIndex = 50;
            this.testo24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo25
            // 
            this.testo25.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo25.AutoSize = true;
            this.testo25.Location = new System.Drawing.Point(236, 127);
            this.testo25.Name = "testo25";
            this.testo25.Size = new System.Drawing.Size(120, 20);
            this.testo25.TabIndex = 51;
            this.testo25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo27
            // 
            this.testo27.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo27.AutoSize = true;
            this.testo27.Location = new System.Drawing.Point(236, 169);
            this.testo27.Name = "testo27";
            this.testo27.Size = new System.Drawing.Size(120, 20);
            this.testo27.TabIndex = 52;
            this.testo27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo28
            // 
            this.testo28.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo28.AutoSize = true;
            this.testo28.Location = new System.Drawing.Point(236, 190);
            this.testo28.Name = "testo28";
            this.testo28.Size = new System.Drawing.Size(120, 20);
            this.testo28.TabIndex = 53;
            this.testo28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo29
            // 
            this.testo29.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo29.AutoSize = true;
            this.testo29.Location = new System.Drawing.Point(236, 211);
            this.testo29.Name = "testo29";
            this.testo29.Size = new System.Drawing.Size(120, 20);
            this.testo29.TabIndex = 54;
            this.testo29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testo26
            // 
            this.testo26.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testo26.AutoSize = true;
            this.testo26.Location = new System.Drawing.Point(236, 148);
            this.testo26.Name = "testo26";
            this.testo26.Size = new System.Drawing.Size(120, 20);
            this.testo26.TabIndex = 55;
            this.testo26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Screen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 421);
            this.Controls.Add(this.tabella3);
            this.Controls.Add(this.tabella2);
            this.Controls.Add(this.tabella1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(400, 460);
            this.MinimumSize = new System.Drawing.Size(400, 460);
            this.Name = "Screen";
            this.Text = "Poker Probability";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabella1.ResumeLayout(false);
            this.tabella1.PerformLayout();
            this.tabella2.ResumeLayout(false);
            this.tabella2.PerformLayout();
            this.tabella3.ResumeLayout(false);
            this.tabella3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tabella1;
        private System.Windows.Forms.Label testo1;
        private System.Windows.Forms.Button calcola;
        private System.Windows.Forms.TableLayoutPanel tabella2;
        private System.Windows.Forms.Label testo2;
        private System.Windows.Forms.Button resetta;
        private System.Windows.Forms.TableLayoutPanel tabella3;
        private System.Windows.Forms.Label testo3;
        private System.Windows.Forms.Label testo19;
        private System.Windows.Forms.Label testo18;
        private System.Windows.Forms.Label testo17;
        private System.Windows.Forms.Label testo16;
        private System.Windows.Forms.Label testo15;
        private System.Windows.Forms.Label testo14;
        private System.Windows.Forms.Label testo13;
        private System.Windows.Forms.Label testo12;
        private System.Windows.Forms.Label testo11;
        private System.Windows.Forms.Label testo00;
        private System.Windows.Forms.Label testo01;
        private System.Windows.Forms.Label testo02;
        private System.Windows.Forms.Label testo03;
        private System.Windows.Forms.Label testo04;
        private System.Windows.Forms.Label testo05;
        private System.Windows.Forms.Label testo06;
        private System.Windows.Forms.Label testo07;
        private System.Windows.Forms.Label testo08;
        private System.Windows.Forms.Label testo09;
        private System.Windows.Forms.Label testo10;
        private System.Windows.Forms.ComboBox seme1;
        private System.Windows.Forms.ComboBox seme2;
        private System.Windows.Forms.ComboBox seme3;
        private System.Windows.Forms.ComboBox seme4;
        private System.Windows.Forms.ComboBox seme5;
        private System.Windows.Forms.ComboBox seme6;
        private System.Windows.Forms.ComboBox seme7;
        private System.Windows.Forms.ComboBox carta2;
        private System.Windows.Forms.ComboBox carta1;
        private System.Windows.Forms.ComboBox carta7;
        private System.Windows.Forms.ComboBox carta6;
        private System.Windows.Forms.ComboBox carta5;
        private System.Windows.Forms.ComboBox carta4;
        private System.Windows.Forms.ComboBox carta3;
        private System.Windows.Forms.Button random;
        private System.Windows.Forms.Label testo4;
        private System.Windows.Forms.Label testo26;
        private System.Windows.Forms.Label testo20;
        private System.Windows.Forms.Label testo21;
        private System.Windows.Forms.Label testo22;
        private System.Windows.Forms.Label testo23;
        private System.Windows.Forms.Label testo24;
        private System.Windows.Forms.Label testo25;
        private System.Windows.Forms.Label testo27;
        private System.Windows.Forms.Label testo28;
        private System.Windows.Forms.Label testo29;
    }
}

