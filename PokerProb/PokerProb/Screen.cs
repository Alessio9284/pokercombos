﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using PokerProb.classes;

namespace PokerProb
{
    public partial class Screen : Form
    {
        private List<card> totale;

        public Screen()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            reset();
        }

        private void reset()
        {
            testo10.Text = "0%";
            testo11.Text = "0%";
            testo12.Text = "0%";
            testo13.Text = "0%";
            testo14.Text = "0%";
            testo15.Text = "0%";
            testo16.Text = "0%";
            testo17.Text = "0%";
            testo18.Text = "0%";
            testo19.Text = "0%";

            testo20.Text = "";
            testo21.Text = "";
            testo22.Text = "";
            testo23.Text = "";
            testo24.Text = "";
            testo25.Text = "";
            testo26.Text = "";
            testo27.Text = "";
            testo28.Text = "";
            testo29.Text = "";
        }

        private void calcola_Click(object sender, EventArgs e)
        {
            block_unblock_buttons();

            card[] mano =
            {
                new card(carta1.Text, seme1.Text),
                new card(carta2.Text, seme2.Text)
            };

            card[] tavolo =
            {
                new card(carta3.Text, seme3.Text),
                new card(carta4.Text, seme4.Text),
                new card(carta5.Text, seme5.Text),
                new card(carta6.Text, seme6.Text),
                new card(carta7.Text, seme7.Text)
            };

            totale = mano.Concat(tavolo).ToList();

            if(checkCards())
            {
                block_unblock_buttons();
                MessageBox.Show("Errore di duplicazione", "ERRORE");
                return;
            }

            reset();

            score p = new score(totale);
            int[] percentuali = { };
            bool completo = false;

            if(totale.Count < 2)
            {
                block_unblock_buttons();
                MessageBox.Show("Inserisci almeno due carte", "ERRORE");
                return;
            }

            switch(totale.Count)
            {
                case 2: percentuali = p.carta5(); break;
                case 3: percentuali = p.carta4(); break;
                case 4: percentuali = p.carta3(); break;
                case 5: percentuali = p.carta2(); break;
                case 6: percentuali = p.carta1(); break;
                case 7: completo = true; break;
                default: throw new Exception();
            }

            if (!completo)
            {
                testo10.Text = ((float)percentuali[0] / percentuali[10] * 100).ToString() + "%";
                testo11.Text = ((float)percentuali[1] / percentuali[10] * 100).ToString() + "%";
                testo12.Text = ((float)percentuali[2] / percentuali[10] * 100).ToString() + "%";
                testo13.Text = ((float)percentuali[3] / percentuali[10] * 100).ToString() + "%";
                testo14.Text = ((float)percentuali[4] / percentuali[10] * 100).ToString() + "%";
                testo15.Text = ((float)percentuali[5] / percentuali[10] * 100).ToString() + "%";
                testo16.Text = ((float)percentuali[6] / percentuali[10] * 100).ToString() + "%";
                testo17.Text = ((float)percentuali[7] / percentuali[10] * 100).ToString() + "%";
                testo18.Text = ((float)percentuali[8] / percentuali[10] * 100).ToString() + "%";
                testo19.Text = ((float)percentuali[9] / percentuali[10] * 100).ToString() + "%";

                testo20.Text = (percentuali[0]).ToString();
                testo21.Text = (percentuali[1]).ToString();
                testo22.Text = (percentuali[2]).ToString();
                testo23.Text = (percentuali[3]).ToString();
                testo24.Text = (percentuali[4]).ToString();
                testo25.Text = (percentuali[5]).ToString();
                testo26.Text = (percentuali[6]).ToString();
                testo27.Text = (percentuali[7]).ToString();
                testo28.Text = (percentuali[8]).ToString();
                testo29.Text = (percentuali[9]).ToString();
            }
            else
            {
                switch (p.puntoGiocatore())
                {
                    case 1: testo10.Text = "100%"; break;
                    case 2: testo11.Text = "100%"; break;
                    case 3: testo12.Text = "100%"; break;
                    case 4: testo13.Text = "100%"; break;
                    case 5: testo14.Text = "100%"; break;
                    case 6: testo15.Text = "100%"; break;
                    case 7: testo16.Text = "100%"; break;
                    case 8: testo17.Text = "100%"; break;
                    case 9: testo18.Text = "100%"; break;
                    case 10: testo19.Text = "100%"; break;
                }
            }

            block_unblock_buttons();
        }

        private void resetta_Click(object sender, EventArgs e)
        {
            block_unblock_buttons();

            carta1.SelectedIndex = -1;
            seme1.SelectedIndex = -1;
            carta2.SelectedIndex = -1;
            seme2.SelectedIndex = -1;
            carta3.SelectedIndex = -1;
            seme3.SelectedIndex = -1;
            carta4.SelectedIndex = -1;
            seme4.SelectedIndex = -1;
            carta5.SelectedIndex = -1;
            seme5.SelectedIndex = -1;
            carta6.SelectedIndex = -1;
            seme6.SelectedIndex = -1;
            carta7.SelectedIndex = -1;
            seme7.SelectedIndex = -1;

            reset();

            block_unblock_buttons();
        }

        private void random_Click(object sender, EventArgs e)
        {
            block_unblock_buttons();

            score s = new score();

            card c = s.getRandomCard();
            carta1.SelectedItem = c.figura;
            seme1.SelectedItem = c.seme;
            c = s.getRandomCard();
            carta2.SelectedItem = c.figura;
            seme2.SelectedItem = c.seme;
            c = s.getRandomCard();
            carta3.SelectedItem = c.figura;
            seme3.SelectedItem = c.seme;
            c = s.getRandomCard();
            carta4.SelectedItem = c.figura;
            seme4.SelectedItem = c.seme;
            c = s.getRandomCard();
            carta5.SelectedItem = c.figura;
            seme5.SelectedItem = c.seme;
            c = s.getRandomCard();
            carta6.SelectedItem = c.figura;
            seme6.SelectedItem = c.seme;
            c = s.getRandomCard();
            carta7.SelectedItem = c.figura;
            seme7.SelectedItem = c.seme;

            block_unblock_buttons();
        }

        private bool checkCards()
        {
            int i;

            while ((i = totale.FindIndex(x => x.numero == 0)) >= 0)
            {
                totale.RemoveAt(i);
            }

            while ((i = totale.FindIndex(x => x.seme == "")) >= 0)
            {
                totale.RemoveAt(i);
            }

            return totale.GroupBy(x => new { x.numero, x.seme }).Where(x => x.Skip(1).Any()).ToList().Count > 0;
        }

        private void block_unblock_buttons()
        {
            calcola.Enabled = !calcola.Enabled;
            resetta.Enabled = !resetta.Enabled;
            random.Enabled = !random.Enabled;
        }
    }
}
