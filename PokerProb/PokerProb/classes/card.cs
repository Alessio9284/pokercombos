﻿using System;

namespace PokerProb.classes
{
    class card
    {
        public int numero;
        public string figura;
        public string seme;

        public card(string n, string s)
        {
            // Se la stringa è vuota la carta non è stata inserita
            numero = (n != "" ? setupfigura(n) : 0);
            figura = n;
            
            seme = s;
        }

        private int setupfigura(string numero)
        {
            switch(numero)
            {
                case "A": return 14;
                case "K": return 13;
                case "Q": return 12;
                case "J": return 11;
                default: return Convert.ToInt32(numero);
            }
        }

        public string setupseme()
        {
            switch (seme)
            {
                /*case "Cuori": return "♥";
                case "Quadri": return "♦";
                case "Fiori": return "♣";
                case "Picche": return "♠";*/
                case "Cuori": return "c";
                case "Quadri": return "q";
                case "Fiori": return "f";
                case "Picche": return "p";
                default: return "ERRORE";
            }
        }

        public card confronto(card c)
        {
            if(numero == c.numero)
            {
                return null;
            }
            else if(numero > c.numero)
            {
                return this;
            }
            else
            {
                return c;
            }
        }
    }
}
