﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PokerProb.classes
{
    class score
    {
        private List<card> carte;
        private List<card> mazzo = new List<card>(52);
        private List<card> mazzoperrandom = new List<card>(52);
        private Random rnd = new Random();

        private int[] punti = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};

        public score()
        {
            initMazzo(mazzoperrandom);
        }

        public score(List<card> totale)
        {
            carte = new List<card>(totale);

            initMazzo(mazzo);
        }

        public void initMazzo(List<card> m)
        {
            string[] carte = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };
            string[] colori = { "Cuori", "Quadri", "Fiori", "Picche" };

            m.Clear();

            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 13; y++)
                {
                    m.Add(new card(carte[y], colori[x]));
                }
            }
        }

        public card getRandomCard()
        {
            int rand = rnd.Next(mazzoperrandom.Count);
            card c = mazzoperrandom.ElementAt(rand);
            mazzoperrandom.RemoveAt(rand);
            return c;
        }

        public int puntoGiocatore()
        {
            /*carte.Sort((x, y) => x.numero.CompareTo(y.numero));

            if (carte.Count == 2)
            {
                if (coppia()) return 2;

                return 1;
            }
            else
            {*/
                carte.Sort((x, y) => x.numero.CompareTo(y.numero));
                carte.Sort((x, y) => x.seme.CompareTo(y.seme));

                if (scalaReale()) return 10;
                if (scalaColore()) return 9;
                if (colore()) return 6;

                carte.Sort((x, y) => x.numero.CompareTo(y.numero));

                if (scalaAsso()) return 5;
                if (scala()) return 5;
                if (poker()) return 8;
                if (full()) return 7;
                if (tris()) return 4;
                if (doppiaCoppia()) return 3;
                if (coppia()) return 2;

                return 1;
            //}
        }

        private bool scalaReale() // 100% funzionante
        {
            int c1 = carte.FindIndex(x => x.numero == 10);
            int c2 = carte.FindIndex(x => x.numero == 11);
            int c3 = carte.FindIndex(x => x.numero == 12);
            int c4 = carte.FindIndex(x => x.numero == 13);
            int c5 = carte.FindIndex(x => x.numero == 14);

            if (c1 > -1 && c2 > -1 && c3 > -1 && c4 > -1 && c5 > -1)
            {
                return carte.ElementAt(c1).seme == carte.ElementAt(c2).seme &&
                       carte.ElementAt(c1).seme == carte.ElementAt(c3).seme &&
                       carte.ElementAt(c1).seme == carte.ElementAt(c4).seme &&
                       carte.ElementAt(c1).seme == carte.ElementAt(c5).seme;
            }
            else
            {
                return false;
            }
        }

        private bool scalaColore()
        {
            int ncarte = 1;

            for (int i = carte.Count - 1, j = carte.Count - 2; j >= 0; i--, j--)
            {
                if (carte[j].numero + 1 == carte[i].numero && carte[j].seme == carte[i].seme) ncarte++;
                else if (ncarte != 1) ncarte = 1;
                if (ncarte == 5) break;
            }

            return ncarte == 5;
        }

        private bool scalaAsso() // 100% funzionante
        {
            int c1 = carte.FindIndex(x => x.numero == 2);
            int c2 = carte.FindIndex(x => x.numero == 3);
            int c3 = carte.FindIndex(x => x.numero == 4);
            int c4 = carte.FindIndex(x => x.numero == 5);
            int c5 = carte.FindIndex(x => x.numero == 14);

            return c1 > -1 && c2 > -1 && c3 > -1 && c4 > -1 && c5 > -1;
        }

        private bool scala() // 100% funzionante
        {
            int ncarte = 1;

            for (int i = carte.Count - 1, j = carte.Count - 2; j >= 0; i--, j--)
            {
                if (carte[j].numero + 1 == carte[i].numero) ncarte++;
                else if (carte[j].numero + 1 < carte[i].numero) ncarte = 1;
                if (ncarte == 5) break;
            }

            return ncarte == 5;
        }

        private bool colore() // 100% funzionante
        {
            int ncarte = 1;

            for (int i = carte.Count - 1, j = carte.Count - 2; j >= 0; i--, j--)
            {
                if (carte[j].seme == carte[i].seme) ncarte++;
                else ncarte = 1;
                if (ncarte == 5) break;
            }

            return ncarte == 5;
        }

        private bool full() // 100% funzionante
        {
            bool coppia = false;
            bool tris = false;

            List<card> c = carte.ToList();

            for (int i = c.Count - 1, j = c.Count - 2, y = c.Count - 3; y >= 0; i--, j--, y--)
            {
                if (c[i].numero == c[j].numero && c[j].numero == c[y].numero)
                {
                    c.RemoveAt(i);
                    c.RemoveAt(j);
                    c.RemoveAt(y);
                    tris = true;
                    break;
                }
            }

            for (int i = c.Count - 1, j = c.Count - 2; j >= 0; i--, j--)
            {
                if (c[i].numero == c[j].numero)
                {
                    coppia = true;
                    break;
                }
            }

            return coppia && tris;
        }

        private bool poker() // 100% funzionante
        {
            bool poker = false;

            for (int i = carte.Count - 1, j = carte.Count - 2, y = carte.Count - 3, x = carte.Count - 4; x >= 0; i--, j--, y--, x--)
            {
                if (carte[i].numero == carte[j].numero && carte[j].numero == carte[y].numero && carte[y].numero == carte[x].numero)
                {
                    poker = true;
                    break;
                }
            }

            return poker;
        }

        private bool tris() // 100% funzionante
        {
            bool tris = false;

            for (int i = carte.Count - 1, j = carte.Count - 2, y = carte.Count - 3; y >= 0; i--, j--, y--)
            {
                if (carte[i].numero == carte[j].numero && carte[j].numero == carte[y].numero)
                {
                    tris = true;
                    break;
                }
            }

            return tris;
        }

        private bool doppiaCoppia() // 100% funzionante
        {
            bool coppia1 = false;
            bool coppia2 = false;

            List<card> c = carte.ToList();

            for (int i = c.Count - 1, j = c.Count - 2, y = c.Count - 3; j >= 0; i--, j--)
            {
                if (c[i].numero == c[j].numero)
                {
                    c.RemoveAt(i);
                    c.RemoveAt(j);
                    coppia1 = true;
                    break;
                }
            }

            for (int i = c.Count - 1, j = c.Count - 2; j >= 0; i--, j--)
            {
                if (c[i].numero == c[j].numero)
                {
                    coppia2 = true;
                    break;
                }
            }

            return coppia1 && coppia2;
        }

        private bool coppia() // 100% funzionante
        {
            bool coppia = false;

            for (int i = carte.Count - 1, j = carte.Count - 2; j >= 0; i--, j--)
            {
                if (carte[i].numero == carte[j].numero)
                {
                    coppia = true;
                    break;
                }
            }

            return coppia;
        }

        public int[] carta1()
        {
            foreach (card c in carte)
            {
                mazzo.RemoveAt(mazzo.FindIndex(x => x.numero == c.numero && x.seme == c.seme));
            }

            foreach (card c in mazzo)
            {
                carte.Add(c);

                punti[puntoGiocatore() - 1]++;

                carte.RemoveAt(carte.FindIndex(x => x.numero == c.numero && x.seme == c.seme));
            }

            punti[10] = mazzo.Count;

            return punti;
        }

        public int[] carta2()
        {
            int comb = 0;

            foreach (card c in carte)
            {
                mazzo.RemoveAt(mazzo.FindIndex(x => x.numero == c.numero && x.seme == c.seme));
            }

            for(int i = 0; i < mazzo.Count; i++)
            {
                card pivot = mazzo.ElementAt(i);
                carte.Add(pivot);

                for (int j = i+1; j < mazzo.Count; j++)
                {
                    card pivot2 = mazzo.ElementAt(j);
                    carte.Add(pivot2);

                    punti[puntoGiocatore() - 1]++;
                    comb++;

                    carte.RemoveAt(carte.FindIndex(x => x.numero == pivot2.numero && x.seme == pivot2.seme));
                }

                carte.RemoveAt(carte.FindIndex(x => x.numero == pivot.numero && x.seme == pivot.seme));
            }

            punti[10] = comb;

            return punti;
        }

        public int[] carta3()
        {
            int comb = 0;

            foreach (card c in carte)
            {
                mazzo.RemoveAt(mazzo.FindIndex(x => x.numero == c.numero && x.seme == c.seme));
            }

            for (int i = 0; i < mazzo.Count; i++)
            {
                card pivot = mazzo.ElementAt(i);
                carte.Add(pivot);

                for (int j = i+1; j < mazzo.Count; j++)
                {
                    card pivot2 = mazzo.ElementAt(j);
                    carte.Add(pivot2);

                    for (int y = j+1; y < mazzo.Count; y++)
                    {
                        card pivot3 = mazzo.ElementAt(y);
                        carte.Add(pivot3);

                        punti[puntoGiocatore() - 1]++;
                        comb++;

                        carte.RemoveAt(carte.FindIndex(x => x.numero == pivot3.numero && x.seme == pivot3.seme));
                    }

                    carte.RemoveAt(carte.FindIndex(x => x.numero == pivot2.numero && x.seme == pivot2.seme));
                }

                carte.RemoveAt(carte.FindIndex(x => x.numero == pivot.numero && x.seme == pivot.seme));
            }

            punti[10] = comb;

            return punti;
        }

        public int[] carta4()
        {
            int comb = 0;

            foreach (card c in carte)
            {
                mazzo.RemoveAt(mazzo.FindIndex(x => x.numero == c.numero && x.seme == c.seme));
            }

            for (int i = 0; i < mazzo.Count; i++)
            {
                card pivot = mazzo.ElementAt(i);
                carte.Add(pivot);

                for (int j = i+1; j < mazzo.Count; j++)
                {
                    card pivot2 = mazzo.ElementAt(j);
                    carte.Add(pivot2);

                    for (int y = j+1; y < mazzo.Count; y++)
                    {
                        card pivot3 = mazzo.ElementAt(y);
                        carte.Add(pivot3);

                        for (int z = y+1; z < mazzo.Count; z++)
                        {
                            card pivot4 = mazzo.ElementAt(z);
                            carte.Add(pivot4);

                            punti[puntoGiocatore() - 1]++;
                            comb++;

                            carte.RemoveAt(carte.FindIndex(x => x.numero == pivot4.numero && x.seme == pivot4.seme));
                        }

                        carte.RemoveAt(carte.FindIndex(x => x.numero == pivot3.numero && x.seme == pivot3.seme));
                    }

                    carte.RemoveAt(carte.FindIndex(x => x.numero == pivot2.numero && x.seme == pivot2.seme));
                }

                carte.RemoveAt(carte.FindIndex(x => x.numero == pivot.numero && x.seme == pivot.seme));
            }

            punti[10] = comb;

            return punti;
        }

        public int[] carta5()
        {
            int comb = 0;

            foreach (card c in carte)
            {
                mazzo.RemoveAt(mazzo.FindIndex(x => x.numero == c.numero && x.seme == c.seme));
            }

            for (int i = 0; i < mazzo.Count; i++)
            {
                card pivot = mazzo.ElementAt(i);
                carte.Add(pivot);

                for (int j = i+1; j < mazzo.Count; j++)
                {
                    card pivot2 = mazzo.ElementAt(j);
                    carte.Add(pivot2);

                    for (int y = j+1; y < mazzo.Count; y++)
                    {
                        card pivot3 = mazzo.ElementAt(y);
                        carte.Add(pivot3);

                        for (int z = y+1; z < mazzo.Count; z++)
                        {
                            card pivot4 = mazzo.ElementAt(z);
                            carte.Add(pivot4);

                            for (int w = z+1; w < mazzo.Count; w++)
                            {
                                card pivot5 = mazzo.ElementAt(w);
                                carte.Add(pivot5);

                                //if (puntoGiocatore() == 10) stampaArray(carte);

                                punti[puntoGiocatore() - 1]++;
                                comb++;

                                carte.RemoveAt(carte.FindIndex(x => x.numero == pivot5.numero && x.seme == pivot5.seme));
                            }

                            carte.RemoveAt(carte.FindIndex(x => x.numero == pivot4.numero && x.seme == pivot4.seme));
                        }

                        carte.RemoveAt(carte.FindIndex(x => x.numero == pivot3.numero && x.seme == pivot3.seme));
                    }

                    carte.RemoveAt(carte.FindIndex(x => x.numero == pivot2.numero && x.seme == pivot2.seme));
                }

                carte.RemoveAt(carte.FindIndex(x => x.numero == pivot.numero && x.seme == pivot.seme));
            }

            punti[10] = comb;

            return punti;
        }

        private void stampaArray(List<card> carte)
        {
            foreach (card carta in carte)
            {
                Console.Write(carta.figura + carta.setupseme() + " ");
            }
            Console.WriteLine();
        }
    }
}
