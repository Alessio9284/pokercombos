﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDD
{
    class Program
    {
        static private int[] indici = new int[7];
        static private List<card> carte = new List<card>(7);
        static private List<card> mazzo = new List<card>(52);
        static private Random rand = new Random();


        static void Main(string[] args)
        {
            initMazzo();

            // inserire qua i test
            customCardTestScala();

            Console.ReadLine();
        }

        static public string puntoGiocatore()
        {
            carte.Sort((x, y) => x.numero.CompareTo(y.numero));
            carte.Sort((x, y) => x.seme.CompareTo(y.seme));

            //if (scalaReale()) return "Scala Reale";
            //if (scalaColore()) return "Scala Colore";
            //if (colore()) return "Colore";

            carte.Sort((x, y) => x.numero.CompareTo(y.numero));

            //if (scalaAsso()) return "Scala";
            if (scala()) return "Scala";
            //if (poker()) return "Poker";
            //if (full()) return "Full";
            //if (tris()) return "Tris";
            //if (doppiaCoppia()) return "Doppia Coppia";
            //if (coppia()) return "Coppia";

            return "Carta Alta";
        }

        /*static public void customCardtestScalaColore()
        {
            carte.Add(new card("2", "♥"));
            carte.Add(new card("3", "♥"));
            carte.Add(new card("4", "♥"));
            carte.Add(new card("5", "♥"));
            carte.Add(new card("6", "♥"));
            carte.Add(new card("9", "♦"));
            carte.Add(new card("K", "♦"));

            // 2 3 4 5 6 9 K sinistra

            foreach (card carta in carte)
            {
                Console.Write(carta.figura() + carta.seme + " ");
            }
            Console.WriteLine(puntoGiocatore());

            carte.Clear();

            carte.Add(new card("2", "♣"));
            carte.Add(new card("5", "♣"));
            carte.Add(new card("7", "♥"));
            carte.Add(new card("8", "♥"));
            carte.Add(new card("9", "♥"));
            carte.Add(new card("10", "♥"));
            carte.Add(new card("J", "♥"));

            // 2 5 7 8 9 10 J destra

            foreach (card carta in carte)
            {
                Console.Write(carta.figura() + carta.seme + " ");
            }
            Console.WriteLine(puntoGiocatore());

            carte.Clear();

            carte.Add(new card("2", "♣"));
            carte.Add(new card("8", "♥"));
            carte.Add(new card("9", "♥"));
            carte.Add(new card("10", "♥"));
            carte.Add(new card("J", "♥"));
            carte.Add(new card("Q", "♥"));
            carte.Add(new card("A", "♣"));

            // 2 8 9 10 J Q A centro

            foreach (card carta in carte)
            {
                Console.Write(carta.figura() + carta.seme + " ");
            }
            Console.WriteLine(puntoGiocatore());

            carte.Clear();

            carte.Add(new card("2", "♣"));
            carte.Add(new card("3", "♣"));
            carte.Add(new card("4", "♣"));
            carte.Add(new card("4", "♦"));
            carte.Add(new card("5", "♣"));
            carte.Add(new card("6", "♣"));
            carte.Add(new card("9", "♦"));

            // 2 3 4 4 5 6 9 doppioni

            foreach (card carta in carte)
            {
                Console.Write(carta.figura() + carta.seme + " ");
            }
            Console.WriteLine(puntoGiocatore());

            carte.Clear();
        }
        */
        static public void customCardTestScala()
        {
            carte.Add(new card("2", "♣"));
            carte.Add(new card("3", "♥"));
            carte.Add(new card("4", "♠"));
            carte.Add(new card("5", "♦"));
            carte.Add(new card("6", "♦"));
            carte.Add(new card("9", "♥"));
            carte.Add(new card("K", "♥"));

            // 2 3 4 5 6 9 K sinistra

            foreach (card carta in carte)
            {
                Console.Write(carta.figura() + carta.seme + " ");
            }
            Console.WriteLine(puntoGiocatore());

            carte.Clear();

            carte.Add(new card("2", "♣"));
            carte.Add(new card("5", "♥"));
            carte.Add(new card("7", "♠"));
            carte.Add(new card("8", "♦"));
            carte.Add(new card("9", "♦"));
            carte.Add(new card("10", "♥"));
            carte.Add(new card("J", "♥"));

            // 2 5 7 8 9 10 J destra

            foreach (card carta in carte)
            {
                Console.Write(carta.figura() + carta.seme + " ");
            }
            Console.WriteLine(puntoGiocatore());

            carte.Clear();

            carte.Add(new card("2", "♣"));
            carte.Add(new card("8", "♥"));
            carte.Add(new card("9", "♠"));
            carte.Add(new card("10", "♦"));
            carte.Add(new card("J", "♦"));
            carte.Add(new card("Q", "♥"));
            carte.Add(new card("A", "♥"));

            // 2 8 9 10 J Q A centro

            foreach (card carta in carte)
            {
                Console.Write(carta.figura() + carta.seme + " ");
            }
            Console.WriteLine(puntoGiocatore());

            carte.Clear();

            carte.Add(new card("2", "♣"));
            carte.Add(new card("3", "♥"));
            carte.Add(new card("4", "♠"));
            carte.Add(new card("4", "♦"));
            carte.Add(new card("5", "♦"));
            carte.Add(new card("6", "♥"));
            carte.Add(new card("9", "♦"));

            // 2 3 4 4 5 6 9 doppioni

            foreach (card carta in carte)
            {
                Console.Write(carta.figura() + carta.seme + " ");
            }
            Console.WriteLine(puntoGiocatore());

            carte.Clear();

            carte.Add(new card("2", "♣"));
            carte.Add(new card("3", "♥"));
            carte.Add(new card("4", "♠"));
            carte.Add(new card("10", "♦"));
            carte.Add(new card("J", "♦"));
            carte.Add(new card("Q", "♥"));
            carte.Add(new card("K", "♦"));

            // 2 3 4 10 J Q K numeri in fila

            foreach (card carta in carte)
            {
                Console.Write(carta.figura() + carta.seme + " ");
            }
            Console.WriteLine(puntoGiocatore());

            carte.Clear();
        }
        /*
        static public void randomCardTest()
        {
            string punto;

            for (int i = 0; i < 10; i++)
            {
                initIndici();

                carte.Add(mazzo[indici[0]]);
                carte.Add(mazzo[indici[1]]);
                carte.Add(mazzo[indici[2]]);
                carte.Add(mazzo[indici[3]]);
                carte.Add(mazzo[indici[4]]);
                carte.Add(mazzo[indici[5]]);
                carte.Add(mazzo[indici[6]]);

                carte.Sort((x, y) => x.numero.CompareTo(y.numero));

                punto = puntoGiocatore();

                //if ((punto == "Carta Alta") --i;
                //else
                ///{
                //    foreach (card carta in carte)
                //    {
                //        Console.Write(carta.figura() + carta.seme + " ");
                //    }
                //    Console.WriteLine(punto);
                //}

                foreach (card carta in carte)
                {
                    Console.Write(carta.figura() + carta.seme + " ");
                }
                Console.WriteLine(punto);

                carte.Clear();
            }
        }*/

        static public void initMazzo()
        {
            string[] carte = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };
            string[] colori = { "♥", "♦", "♣", "♠" };

            mazzo.Clear();

            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 13; y++)
                {
                    mazzo.Add(new card(carte[y], colori[x]));
                }
            }
        }

        static public void initIndici()
        {
            int rnd;

            for(int i = 0; i < 7; i++)
            {
                while (Array.IndexOf(indici, rnd = rand.Next(0, 52)) != -1) { }

                indici[i] = rnd;
            }
        }

        static private bool scalaReale()
        {
            int c1 = carte.FindIndex(x => x.numero == 10);
            int c2 = carte.FindIndex(x => x.numero == 11);
            int c3 = carte.FindIndex(x => x.numero == 12);
            int c4 = carte.FindIndex(x => x.numero == 13);
            int c5 = carte.FindIndex(x => x.numero == 14);

            if (c1 > -1 && c2 > -1 && c3 > -1 && c4 > -1 && c5 > -1)
            {
                return carte.ElementAt(c1).seme == carte.ElementAt(c2).seme &&
                       carte.ElementAt(c1).seme == carte.ElementAt(c3).seme &&
                       carte.ElementAt(c1).seme == carte.ElementAt(c4).seme &&
                       carte.ElementAt(c1).seme == carte.ElementAt(c5).seme;
            }
            else
            {
                return false;
            }
        }

        static private bool scalaColore()
        {
            int ncarte = 1;

            for (int i = carte.Count - 1, j = carte.Count - 2; j >= 0; i--, j--)
            {
                if (carte[j].numero + 1 == carte[i].numero && carte[j].seme == carte[i].seme) ncarte++;
                else if (ncarte != 1) ncarte = 1;
                if (ncarte == 5) break;
            }

            return ncarte == 5;
        }

        static private bool scalaAsso()
        {
            int c1 = carte.FindIndex(x => x.numero == 2);
            int c2 = carte.FindIndex(x => x.numero == 3);
            int c3 = carte.FindIndex(x => x.numero == 4);
            int c4 = carte.FindIndex(x => x.numero == 5);
            int c5 = carte.FindIndex(x => x.numero == 14);

            return c1 > -1 && c2 > -1 && c3 > -1 && c4 > -1 && c5 > -1;
        }

        static private bool scala()
        {
            int ncarte = 1;

            for (int i = carte.Count - 1, j = carte.Count - 2; j >= 0; i--, j--)
            {
                if (carte[j].numero + 1 == carte[i].numero) ncarte++;
                else if (carte[j].numero + 1 < carte[i].numero) ncarte = 1;
                if (ncarte == 5) break;
            }

            return ncarte == 5;
        }

        static private bool colore()
        {
            int ncarte = 1;

            for (int i = carte.Count - 1, j = carte.Count - 2; j >= 0; i--, j--)
            {
                if (carte[j].seme == carte[i].seme) ncarte++;
                else ncarte = 1;
                if (ncarte == 5) break;
            }

            return ncarte == 5;
        }

        static private bool full()
        {
            bool coppia = false;
            bool tris = false;

            List<card> c = carte.ToList();

            for (int i = c.Count - 1, j = c.Count - 2, y = c.Count - 3; y >= 0; i--, j--, y--)
            {
                if (c[i].numero == c[j].numero && c[j].numero == c[y].numero)
                {
                    c.RemoveAt(i);
                    c.RemoveAt(j);
                    c.RemoveAt(y);
                    tris = true;
                    break;
                }
            }

            for (int i = c.Count - 1, j = c.Count - 2; j >= 0; i--, j--)
            {
                if (c[i].numero == c[j].numero)
                {
                    coppia = true;
                    break;
                }
            }

            return coppia && tris;
        }

        static private bool poker()
        {
            bool poker = false;

            for (int i = carte.Count - 1, j = carte.Count - 2, y = carte.Count - 3, x = carte.Count - 4; x >= 0; i--, j--, y--, x--)
            {
                if (carte[i].numero == carte[j].numero && carte[j].numero == carte[y].numero && carte[y].numero == carte[x].numero)
                {
                    poker = true;
                    break;
                }
            }

            return poker;
        }

        static private bool tris()
        {
            bool tris = false;

            for (int i = carte.Count - 1, j = carte.Count - 2, y = carte.Count - 3; y >= 0; i--, j--, y--)
            {
                if (carte[i].numero == carte[j].numero && carte[j].numero == carte[y].numero)
                {
                    tris = true;
                    break;
                }
            }

            return tris;
        }

        static private bool doppiaCoppia()
        {
            bool coppia1 = false;
            bool coppia2 = false;

            List<card> c = carte.ToList();

            for (int i = c.Count - 1, j = c.Count - 2, y = c.Count - 3; j >= 0; i--, j--)
            {
                if (c[i].numero == c[j].numero)
                {
                    c.RemoveAt(i);
                    c.RemoveAt(j);
                    coppia1 = true;
                    break;
                }
            }

            for (int i = c.Count - 1, j = c.Count - 2; j >= 0; i--, j--)
            {
                if (c[i].numero == c[j].numero)
                {
                    coppia2 = true;
                    break;
                }
            }

            return coppia1 && coppia2;
        }

        static private bool coppia()
        {
            bool coppia = false;

            for (int i = carte.Count - 1, j = carte.Count - 2; j >= 0; i--, j--)
            {
                if (carte[i].numero == carte[j].numero)
                {
                    coppia = true;
                    break;
                }
            }

            return coppia;
        }
    }

    class card
    {
        public int numero;
        public string seme;

        public card(string n, string s)
        {
            // Se la stringa è vuota la carta non è stata inserita
            numero = (n != "" ? str_to_int(n) : 0);

            seme = s;
        }

        private int str_to_int(string numero)
        {
            switch (numero)
            {
                case "A": return 14;
                case "K": return 13;
                case "Q": return 12;
                case "J": return 11;
                default: return Convert.ToInt32(numero);
            }
        }

        public string figura()
        {
            switch (numero)
            {
                case 14: return "A";
                case 13: return "K";
                case 12: return "Q";
                case 11: return "J";
                default: return numero.ToString();
            }
        }

        /*public card confronto(card c)
        {
            if (numero == c.numero)
            {
                return null;
            }
            else if (numero > c.numero)
            {
                return this;
            }
            else
            {
                return c;
            }
        }*/
    }
}
